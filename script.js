var app;
var model = {
  isSaving: false,
  phProjectName: "Project Name",
  input: "",
  id: "",
  project: {
    name: "",
    edit: false,
    todos: [{message: "", edit: false}]
  }
};

$(document).ready(function() {


  function addTodo() {
    model.project.todos.push({message: app.input, edit: false});
    app.input = "";
    save();
  }

  function remove(index) {
    model.project.todos.splice(index,1);
    save();
  }

  function up(index) {
    var todo = model.project.todos[index];
    model.project.todos.splice(index, 1);
    model.project.todos.splice(index - 1, 0, todo);
    save();
  }

  function down(index) {
    var todo = model.project.todos[index];
    model.project.todos.splice(index, 1);
    model.project.todos.splice(index + 1, 0, todo);
    save();
  }

  function getList() {
    console.log("getting");
    $.ajax({
      url:"https://api.myjson.com/bins/" + model.id,
      type: "GET",
      cache: false,
      contentType:"application/json; charset=utf-8",
      dataType:"json",
      success: function(data) {
        console.log("Got");
        console.log(data);
        if(data.name !== undefined && data.todos !== undefined && Array.isArray(data.todos)) {
          console.log("Got from id: " + model.id);
          model.project.name = data.name;
          model.project.todos.splice(0);
          data.todos.forEach((todo)=>model.project.todos.push(todo));
        }
        else {
          model.project.name = "";
          model.project.todos.splice(0);
          model.project.todos.push({message: "", edit: false});
        }
      },
      error: function(e,s) {
        console.log(e);
        console.log(s);
        model.project.name = "";
        model.project.todos.splice(0);
        model.project.todos.push({message: "", edit: false});
      }
    });
  }

  function closeEdit() {
    model.project.edit = false;
    model.project.todos.forEach((todo)=>todo.edit = false);
  }

  function save() {
    if(model.id.length > 0) {
      model.isSaving = true;
      $.ajax({
        url: "https://api.myjson.com/bins/" + model.id,
        type: "PUT",
        data: JSON.stringify(model.project),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (rdata, textStatus, jqXHR) {
          console.log("success");
          setTimeout(function(){model.isSaving = false;}, 500);
        },
        error: function() {
          setTimeout(function(){model.isSaving = false;}, 500);
        }
      });
    }
  }

  function saveas() {
    var toSend = JSON.stringify(model.project);
    model.isSaving = true;
    $.ajax({
      url:"https://api.myjson.com/bins",
      type: "POST",
      cache: false,
      data: toSend,
      contentType:"application/json; charset=utf-8",
      dataType:"json",
      success: function(data) {
        var parts = JSON.stringify(data.uri).split('/');
        var lastPart = parts[parts.length - 1];
        lastPart = lastPart.substring(0, lastPart.length - 1);
        console.log("success");
        console.log(lastPart);
        model.id = lastPart;
        setTimeout(function(){model.isSaving = false;}, 500);
      },
      error: function() {
        setTimeout(function(){model.isSaving = false;}, 500);
      }
    });
  }

  app = new Vue({
    el: '#app',
    data: model,
    methods: {
      addTodo: addTodo,
      updateTodo: function(index, editTodo, e) {
        console.log(e);
        console.log(this.project.todos[index]);
        this.project.todos[index].edit = editTodo;
        save();
        if(!editTodo) {
          closeEdit();
        }
        else {
          console.log();
          this.$nextTick(() => $(e.target.parentNode).find(".todoinput").first().focus() );
        }
      },
      remove: remove,
      getList: function(e) {
        if(e.keyCode !== 17) {
          getList();
        }
      },
      setNameEdit: function(editName) {
        this.project.edit = editName;
        save();
        if(!editName) {
          closeEdit();
        }
        else {
          this.$nextTick(() => app.$refs.nameinput.focus());
        }
      },
      save: save,
      saveas: saveas,
      up: up,
      down: down
    }
  });
});
